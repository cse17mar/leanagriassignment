package com.leanagriassignment.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.leanagriassignment.R;
import com.leanagriassignment.base.BaseActivity;

import static com.leanagriassignment.util.AppConstant.KEY_OPEN_VIEW;
import static com.leanagriassignment.util.AppConstant.MOVIE_VIEW;
import static com.leanagriassignment.util.AppConstant.SPLASH_DISPLAY_TIME;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected int getContentResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void init(@Nullable Bundle state) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent launchIntent = new Intent(SplashActivity.this, ActionActivity.class);
                launchIntent.putExtra(KEY_OPEN_VIEW, MOVIE_VIEW);
                startActivity(launchIntent);
                finish();
            }
        }, SPLASH_DISPLAY_TIME);

    }
}

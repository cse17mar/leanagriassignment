package com.leanagriassignment.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leanagriassignment.R;
import com.leanagriassignment.listener.OnDetialsClickedListener;
import com.leanagriassignment.roomdb.MovieData;
import com.leanagriassignment.util.Utils;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.leanagriassignment.util.AppConstant.POSTER_BASE_URL;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    private List<MovieData> movieDataList;
    private Context context;
    private OnDetialsClickedListener onDetialsClickListener;

    public MovieAdapter(List<MovieData> movieDataList, OnDetialsClickedListener onDetialsClickListener) {
        this.onDetialsClickListener = onDetialsClickListener;
        this.movieDataList = movieDataList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemViem = LayoutInflater.from(context).inflate(R.layout.item_movie, parent, false);
        return new ViewHolder(itemViem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDetialsClickListener.onDetails(position);
            }
        });
        holder.tvMovieName.setText(movieDataList.get(position).getTitle());
        holder.rbMovieRating.setRating(movieDataList.get(position).getVoteAverage()/2); // ratted out 10 , but we are showing out of 5
        holder.tvMovieReleaseDate.setText(movieDataList.get(position).getReleaseDate());
        String fullPosterUrlPath = POSTER_BASE_URL + movieDataList.get(position).getPosterPath();
        if(Utils.hasInternetAccess(context)) {
            Picasso.with(context).load(fullPosterUrlPath)
                    .error(R.drawable.ic_place_holder)
                    .into(holder.ivPoster);
        }else {

        }
    }

    @Override
    public int getItemCount() {
        return movieDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        AppCompatImageView ivPoster;
        AppCompatTextView tvMovieName;
        AppCompatTextView tvMovieReleaseDate;
        AppCompatRatingBar rbMovieRating;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ivPoster = itemView.findViewById(R.id.ivPoster);
            tvMovieName = itemView.findViewById(R.id.tvMovieName);
            tvMovieReleaseDate = itemView.findViewById(R.id.tvMovieReleaseDate);
            rbMovieRating = itemView.findViewById(R.id.rbMovieRating);
        }
    }
}

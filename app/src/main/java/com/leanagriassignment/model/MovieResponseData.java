package com.leanagriassignment.model;

import com.google.gson.annotations.SerializedName;
import com.leanagriassignment.roomdb.MovieData;

import java.util.List;

public class MovieResponseData {
    @SerializedName("total_pages")
    private int totalPages;
    @SerializedName("page")
    private int currentPage;

    @SerializedName("results")
    private List<MovieData> movieDataList;

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<MovieData> getMovieDataList() {
        return movieDataList;
    }

    public void setMovieDataList(List<MovieData> movieDataList) {
        this.movieDataList = movieDataList;
    }


    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }
}

package com.leanagriassignment.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.net.ConnectException;

public abstract class BaseFragment extends Fragment {
    private View layoutView;
    protected ProgressDialog mProgressDialog = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            layoutView = inflater.inflate(getContentResource(), container, false);
        } catch (InflateException ex) {
            ex.printStackTrace();
        }
        init(savedInstanceState, layoutView);

        return layoutView;
    }

    protected abstract int getContentResource();

    protected abstract void init(Bundle savedInstanceState, View layoutView);
    protected void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getContext(), null, message, true, false, null);
        } else {
            mProgressDialog.setMessage(message);
            mProgressDialog.show();
        }
    }

    protected void hideProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
    public void showToast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public void errorMsg(Throwable t) {
        hideProgressDialog();
        if (t instanceof ConnectException) {
            showToast("Connection time out");
        }else {
            showToast("Somthing went wrong");        }

    }
}
